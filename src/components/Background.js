import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  background-color: yellow;
  z-index: -1;
`

const Background = () => (
  <Container>
    <span>Background</span>
  </Container>
)

export default Background
