import vgEOT from './VG5000-Regular_web.eot'
import vgWOFF from './VG5000-Regular_web.woff'
import vgWOFF2 from './VG5000-Regular_web.woff2'
import vgTTF from './VG5000-Regular_web.ttf'

import hnglEOT from './vtf_hngl-webfont.eot'
import hnglTTF from './vtf_hngl-webfont.ttf'
import hnglWOFF from './vtf_hngl-webfont.woff'
import hnglSVG from './vtf_hngl-webfont.svg'

import mrpxEOT from './VTFMisterPixel-Tools.eot'
import mrpxTTF from './VTFMisterPixel-Tools.ttf'
import mrpxWOFF from './VTFMisterPixel-Tools.woff'
import mrpxSVG from './VTFMisterPixel-Tools.svg'

import spgtEOT from './Sporting_Grotesque-Bold_web.eot'
import spgtTTF from './Sporting_Grotesque-Bold_web.ttf'
import spgtWOFF from './Sporting_Grotesque-Bold_web.woff'
import spgtWOFF2 from './Sporting_Grotesque-Bold_web.woff2'
import spgtSVG from './Sporting_Grotesque-Bold_web.svg'

import stepEOT from './Steps-Mono-Thin.eot'
import stepSVG from './Steps-Mono-Thin.svg'
import stepTTF from './Steps-Mono-Thin.ttf'
import stepWOFF from './Steps-Mono-Thin.woff'
import stepWOFF2 from './Steps-Mono-Thin.woff2'

import cmnuEOT from './Commune-Nuit_Debout_web.eot'
import cmnuSVG from './Commune-Nuit_Debout_web.svg'
import cmnuTTF from './Commune-Nuit_Debout_web.ttf'
import cmnuWOFF from './Commune-Nuit_Debout_web.woff'
import cmnuWOFF2 from './Commune-Nuit_Debout_web.woff2'

import mlmtEOT from './Millimetre-Regular_web.eot'
import mlmtTTF from './Millimetre-Regular_web.ttf'
import mlmtWOFF from './Millimetre-Regular_web.woff'
import mlmtWOFF2 from './Millimetre-Regular_web.woff2'

import bluuSVG from './bluunext-bold-webfont.svg'
import bluuTTF from './bluunext-bold.ttf'
import bluuWOFF from './bluunext-bold-webfont.woff'
import bluuWOFF2 from './bluunext-bold-webfont.woff2'

export {
  vgEOT,
  vgWOFF,
  vgWOFF2,
  vgTTF,
  hnglEOT,
  hnglTTF,
  hnglWOFF,
  hnglSVG,
  mrpxEOT,
  mrpxTTF,
  mrpxWOFF,
  mrpxSVG,
  spgtEOT,
  spgtTTF,
  spgtWOFF,
  spgtWOFF2,
  spgtSVG,
  stepEOT,
  stepTTF,
  stepWOFF,
  stepWOFF2,
  stepSVG,
  cmnuEOT,
  cmnuSVG,
  cmnuTTF,
  cmnuWOFF,
  cmnuWOFF2,
  mlmtEOT,
  mlmtTTF,
  mlmtWOFF,
  mlmtWOFF2,
  bluuSVG,
  bluuTTF,
  bluuWOFF,
  bluuWOFF2,
}
