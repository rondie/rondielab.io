import React from 'react'
import styled from 'styled-components'

const Question = styled.div`
  color: black;
  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-family: 'vg', sans-serif;
    font-weight: normal;
    font-size: 2rem;
    color: black;
  }
  p {
    font-family: 'vg', sans-serif;
    font-size: 1.5rem;
    line-height: 145%;
  }
  sup {
    font-family: 'vg', sans-serif;
  }
  border-bottom: 3px solid black;
  padding-bottom: 48px;
  margin-bottom: 48px;
`

const Answer = styled.div`
  p,
  li {
    font-family: monospace;
    color: black;
    font-size: 1.25rem;
    line-height: 145%;
  }

  h1,
  h2,
  h3,
  h4,
  h5,
  h6 {
    font-family: 'vg', sans-serif;
    font-weight: normal;
    font-size: 2rem;
    color: black;
  }
`

const List = styled.ul`
  padding: 0;
  li {
    display: inline-block;
    &:not(:last-of-type) {
      &::after {
        content: ',';
        margin-right: 3px;
      }
    }
  }
`

export { Question, Answer, List }
