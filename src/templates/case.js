// import React from 'react'
// import { graphql } from 'gatsby'
// import Img from 'gatsby-image'
// import styled from 'styled-components'
// import media from 'styled-media-query'
// import { Layout, Title, Date, Copy } from '../components'
// import { randomLinkColour, resetLinkColour } from '../functions/randomColour'

// const Banner = styled.div`
//   border-bottom: 1px solid var(--black);
//   padding-bottom: 2rem;
// `

// const Button = styled.div`
//   margin-top: 2rem;
//   padding: 0.75rem 1.5rem;
//   background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAABZJREFUeNpi2r9//38gYGAEESAAEGAAasgJOgzOKCoAAAAASUVORK5CYII=);
//   width: fit-content;
//   transition: all 0.1s;
//   border: 1px solid transparent;
//   a {
//     text-decoration: none;
//     font-size: 1.25rem;
//     font-family: 'IBM Plex Mono', monospace;
//     font-weight: medium;
//   }
//   &::before {
//     content: "9";
//     font-family: 'mrpx';
//     font-size: 1.25rem;
//     margin-right: .75rem;
//   }
//   &:hover {
//     border-color: var(--black);
//   &::before {
//     content: "7";
//   }
//   }
// }
// `

// const Detail = styled.div`
//   display: flex;
//   flex-direction: column;
//   height: 100%;
//   width: 100%;
//   z-index: 2;
//   top: 0;
//   left: 0;
//   justify-content: center;
// `
// const ProjectTitle = styled.h2`
//   font-family: 'bluu', serif;
//   font-size: 5rem;
//   margin: 0;
//   ${media.lessThan('medium')`
//   font-size: 4rem;
//   line-height: 120%;
//   margin-top: 3rem;
//   margin-bottom: 1.5rem;
//   `}
// `

// const ProjectSubtitle = styled.h4`
//   font-family: 'IBM Plex Mono', monospace;
//   font-weight: normal;
//   font-size: 1rem;
//   margin: 0;
// `

// const Image = styled(Img)``

// const Body = styled.div`
//   width: 80%;
//   ${media.lessThan('medium')`
//     width: 100%;
//   `}
//   h3 {
//     margin-top: 2rem;
//     font-family: 'IBM Plex Mono', monospace;
//     font-weight: medium;
//     text-transform: capitalize;
//     font-size: 1.25rem;
//     width: fit-content;
//     position: relative;
//     z-index: 1;
//     &::after {
//       content: '';
//       position: absolute;
//       z-index: -2;
//       background-color: var(--darkgrey);
//       width: calc(100% + 12px);
//       height: calc(100% - 12px);
//       top: 12px;
//       left: -6px;
//       transition: all 0.5s;
//     }
//     &::before {
//       content: '';
//       position: absolute;
//       z-index: -1;
//       background-color: var(--yellow);
//       width: 0px;
//       height: calc(100% - 12px);
//       top: 12px;
//       left: -6px;
//       transition: all 0.5s;
//     }
//     &:hover {
//       &::before {
//         width: calc(100% + 12px);
//       }
//     }
//   }
//   p,
//   li {
//     transition: all 0.5s;
//     font-family: 'IBM Plex Sans', sans-serif;
//     font-size: 1.125rem;
//     ${media.lessThan('medium')`
//     font-size: 1rem;
//   `}
//   }
//   ul {
//     padding-left: 0;
//   }
//   li {
//     position: relative;
//     list-style: none;
//     &::before {
//       position: absolute;
//       font-family: 'mrpx';
//       content: '=';
//       left: -1rem;
//       font-size: 0.5rem;
//       top: 0;
//     }
//   }
// `

// export default function WorkPost({
//   data, // this prop will be injected by the GraphQL query below.
// }) {
//   const { markdownRemark } = data // data.markdownRemark holds our post data
//   const { frontmatter, html } = markdownRemark
//   return (
//     <Layout>
//       <div>
//         <Banner>
//           {/* <Image fluid={frontmatter.thumb.childImageSharp.fluid} /> */}
//           <Detail>
//             <ProjectTitle>{frontmatter.title}</ProjectTitle>
//             <ProjectSubtitle>{frontmatter.intro}</ProjectSubtitle>
//           </Detail>
//           <Button>
//             <a
//               onMouseEnter={randomLinkColour}
//               onMouseLeave={resetLinkColour}
//               href={frontmatter.url}
//               rel="noopener noreferrer"
//               target="_blank"
//             >
//               {frontmatter.cta}
//             </a>
//           </Button>
//         </Banner>
//         <Body dangerouslySetInnerHTML={{ __html: html }} />
//       </div>
//     </Layout>
//   )
// }

// export const workQuery = graphql`
//   query($path: String!) {
//     markdownRemark(frontmatter: { path: { eq: $path } }) {
//       html
//       frontmatter {
//         url
//         cta
//         date(formatString: "MMMM DD, YYYY")
//         path
//         title
//         intro
//         thumb {
//           childImageSharp {
//             fluid(maxWidth: 1080) {
//               base64
//               tracedSVG
//               aspectRatio
//               src
//               srcSet
//               srcWebp
//               srcSetWebp
//               sizes
//               originalImg
//               originalName
//               presentationWidth
//               presentationHeight
//             }
//           }
//         }
//       }
//     }
//   }
// `
