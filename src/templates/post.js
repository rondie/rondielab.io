// import React from 'react'
// import { graphql } from 'gatsby'
// import { Layout, Title, Date, Copy } from '../components'

// const Post = ({ data }) => {
//   const { markdownRemark } = data // data.markdownRemark holds our post data
//   const { frontmatter, html, parent } = markdownRemark
//   const { title, date } = frontmatter
//   const { mtime } = parent
//   return (
//     <Layout>
//       <div>
//         <Title>{title}</Title>
//         <Date>{date}</Date>
//         <Date>{mtime}</Date>
//         <Copy html={html} />
//       </div>
//     </Layout>
//   )
// }

// export const pageQuery = graphql`
//   query($path: String!) {
//     markdownRemark(frontmatter: { path: { eq: $path } }) {
//       html
//       frontmatter {
//         date
//         path
//         title
//         thumb {
//           childImageSharp {
//             original {
//               width
//               height
//               src
//             }
//           }
//         }
//       }
//       parent {
//         ... on File {
//           mtime
//         }
//       }
//     }
//   }
// `

// export default Post
