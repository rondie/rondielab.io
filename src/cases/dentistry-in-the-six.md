---
path: '/work/dentistry-in-the-six'
date: '2019-08-01'
title: 'Dentistry In The 6ix'
subtitle: 'A Gatsby Site'
intro: 'Static webpage built using GatsbyJS and Wordpress API'
thumb: '../images/dentistry-hero.jpg'
published: true
order: 1
domain: 'https://dentistryinthe6ix.com'
url: 'http://squroo.com/'
cta: 'Visit demo site'
---

###Client

Toronto-based dental clinic Dentistry in the 6ix, who were looking for a website “that didn’t look like your average dental clinic website”

###Stack

- Stack: GatsbyJS, Wordpress
- Hosted: DigitalOcean (Ubuntu)
- CI/CL Runner: Gitlab
- Database: Default MySQL

###Process

Performance, simplicity, and flexibility were all important to the client, so I advised that building a static site was the best choice.

Wordpress is a CMS the client was comfortable with, but this website didn’t require to be "dynamic" since the content would not be frequently created or updated. Therefore this site was a good candidate for me to go headless and use the Wordpress API.

I chose Gatsby because it has a good community and support backing it up, it also allows me to build it using ReactJS.

The end result was a lightweight, modern marketing site that is easy to maintain. Without having to worry about theme or plugin updates, the client can focus on running their business and making patients smile.

PS - I also designed the website, too :-)
