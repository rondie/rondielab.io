---
order: 0
path: '/work/cohost-app'
date: '2019-08-01'
title: 'Cohost'
intro: 'Software MVP built with React and suppported by Airtable'
subtitle: 'Software MVP built with React and suppported by Airtable'
published: true
thumb: '../images/cohost-thumbnail.jpg'
url: 'https://cohost-demo.herokuapp.com/'
cta: 'View demo'
---

###Client

Cohost enables hotel staff to unify guest data across hotel systems to paint a single picture of a guests stay. Save time by automating daily tasks and issue resolutions, freeing up time and attention to improve guest experiences.

_Features include:_

- Sort and list as report of reservation status of each hotel room
- Fetch profile data of a user
- Reservations of a specific user and its details
- Housekeeping status of the hotel

###Stack

- Stack: Node(Express), React/Redux
- Platform: [Front](https://frontapp.com/)
- Hosted: Heroku
- CI/CL Runner: Gitlab
- Database: [Airtable](https://airtable.com/)
- Automation: Zapier/Lambda
- Oauth: [Okta](https://www.okta.com/)
- Debug: [Sentry](https://sentry.io/)

###Process

Working with a small startup team, my goal was to quickly and efficiently build an MVP to handle customer data for a handful of boutique hotels in Toronto.

To create the most value in a short amount of time, I decided to leverage existing tools to streamline the process:

I built a custom plugin for [Front](https://frontapp.com/), an inbox management app, through a SPA to handle customer data. Behind the application, I used [Airtable](https://airtable.com/) as a database + API, and Zapier for automation (now Lambda) to handle CRUD operations. The front end was built with React for the sake of speed and flexibility.

Although Airtable was capable to process a solid amount of data, it also had its limitations. Have a node server running was helpful to process additional data. Redux was used to handle the states that serves on the client side.

I learned a lot by working on Cohost, especially automations, by applying scripts to Zapier and using AWS lambda functions later for webhooks.
