---
path: '/work/images-festival'
date: '2019-08-01'
title: 'Images Festival'
subtitle: 'IT & Digital Support'
published: true
order: 2
intro: 'Systems engineering and IT consulting'
url: 'https://imagesfestival.com/'
cta: 'Visit organization site'
---

###Client

Founded in 1987, Images Festival is an artist-driven festival devoted to independent and experimental film, video art, new media and media installation that takes place each spring in Toronto. They are also a registered charity.

###Process

My ongoing consulting work with Images Festival has built my leadership, problem solving, and client services skills.

Images Festival is powered an amazing team of artists, curators and coordinators. I work closely with the team to solve technical challenges and oversee technology decisions.

Over the past year or so, I have been supporting with various tasks including:

**Cybersecurity**  
The festival website was getting hacked almost daily. After auditing the site, I uncovered that it was getting hacked because a very dated version of PHP was used. Additionally, the previous developer who built a custom CMS never wrote any updates for it. This left the site very vulnerable to hackers. Based on my learnings, I recommended migrating to a more secure and current CMS and keeping it up to date.

Since the migration, I’ve been maintaining the linux server, reviewing regularly and making necessary security updates. I’ve also used tools such as fail2ban to monitor and protect against any suspicious visitors.

The festival has not been hacked since!

**Platforms**  
Images was using an obsolete relational database application (FileMaker running on OSX Lion in 2018). The data was only able to be stored on its local server, with no other backups. It also required VPN to access the data outside the local network, which was very inconvenient for the often remote staff. Lastly, updating to a newer version was completely out of their budget.

I researched different options to meet their needs, and ended up choosing Airtable because of accessibility, flexibility, and overall capabilities. Airtable supports cross-device access. It allowed me to tailor the data format based on the old database, while also allowing for new changes. Another bonus was that the learning curve is much faster than FileMaker. Each year, new interns work on data entry, so a faster learning curve means they can be onboarded faster.

Making big changes to the database gave me an opportunity to unify and clean up redundant data. This was super gratifying :-)

**Education**  
I really enjoy working with the team and explaining technical things to people who “don’t think they are technical” (for the record, I think anyone can be technical! It’s all in how you approach things.)

I taught the staff about basic security best practices, how the cloud works, basic HTML and CSS, and more, tailoring each topic and lesson to individual needs.
