---
path: '/work/hotblack-coffee'
date: '2019-08-01'
title: 'Hotblack Coffee'
subtitle: 'Hotblack Coffee'
published: false
order: 3
intro: ''
---

Background:
Images Festival is a not-for-profit art organization in Toronto. Feel free to check out the [website](https://www.imagesfestival.com/)

Tasks:
There is a few main responsiblity working with Images Festival

Maintain website
Site that was built using Laravel + OctoberCMS. I help with solving issues when error happens. I also take care if any developmental needs.

Cybersecurity
As the organization has experienced numerous cyberattacks before. I help with minimizing the risks of the site getting hacked. Mainly on maintaining the server side keeping things are updated and use tools to protect from fail2ban

CRM solution alternative
To find an alternative of Filemaker, the version was really obsolete. Using Airtable to reorganize the existing data.

Provide backup solutions
Consultancy on how to use Google Drive as a cloud backup alongside with external HDD backups

Misc. IT support
