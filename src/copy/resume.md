---
path: '/resume'
title: 'resume'
file: '../images/Rondie_Li_Resume.pdf'
---

Software engineer generalist with 3+ years of experience. I care deeply about user-centric product development; designing and architecting systems that focus on performance, scalability and usability.

###Technical Skills

- Programming languages : Javascript, Python
- Web application frameworks: React, Angular
- Client-side frameworks: Gatsby, NextJS
- Server-side frameworks: Node, Express
- Markup/style languages: HTML5, Markdown, SCSS
- Other tools: Redux, Socket.io, Git, NPM, Webpack, GraphQL, MongoDB, AWS Lambda, Linux/UNIX

###Selected Experience

**Software Engineer, Cohost Hospitality**  
January 2019–August 2019

Cohost is a Toronto-based startup catering to the hospitality industry. As a software engineer, I was responsible for the full lifecycle of software delivery.

- Design and develop front end components using React
- Create, augment, and improve database-backed API services in Node.js.
- Regularly conduct code reviews to ensure high quality outputs
  Collaborate with the project team through technical and requirements gathering working sessions

**Co-Founder and Full Stack Developer, Crrumb**  
April 2015–December 2018

At Crrumb, I worked closely with a small agile team to build and deploy websites and web applications. Our key capabilities include web & web application development, CMS & platform builds, and platform & API integration.

- Translate design team’s wireframes and prototypes into responsive, interactive websites using HTML/CSS and JavaScript
- Proactively identify code deficiencies, recommending solutions to the team and completing them with clean, reusable code
- Provide ongoing guidance and direction to the development team
  Recommend and implement technical solutions for client projects

_Notable projects_

- Developed Single Page Applications using libraries like React and Redux, supported by RESTful APIs and/or GraphQL
- Developed and maintained critical components of e-commerce websites, including shopping cart, quick view, and checkout page, using React components
- Utilized HTML, SCSS, Javascript, Node and Git
- Created custom client side components of web pages and sites

**Freelance UX Designer, Uberflip**  
September 2017–September 2018

At Uberflip, I worked closely with Product Managers, the CTO, and the CEO to design seamless interactions and deliver delightful experiences that make users love Uberflip even more. I also created and contributed to a componentized design system.

- Define the strategy for the Uberflip design system
- Creating the components for the system in collaboration with cross-functional teams
- Documenting the usage guidelines for components and patterns within the system
- Determine the best UX solutions based on customer feedback and business goals and helps to determine overall direction, identifying challenges and adapting as needed
- Rationalize solutions and effectively communicate ideas and designs

**Visual Designer, Media One Creative**  
February 2016–June 2016

At Media One Creative, I worked on numerous projects across digital (web design, landing pages, digital ads, etc.) and print.

- Solve design problems through simple, beautiful design
- Create high fidelity designs and prototypes
- Provide quality assurance on creative deliverables

**Print Operator, Astley Gilbert Limited**  
February 2011–April 2015

At Astley Gilbert, I was responsible for setting up and operating printing equipment and specialized in the printing of architectural drawings.

###Education

Advanced Diploma in Advertising and Marketing, Centennial College  
Toronto, Ontario, Canada  
2015

Diploma in Business Administration, Hong Kong Institute of Vocational Education  
Hong Kong  
2006
