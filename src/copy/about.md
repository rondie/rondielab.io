---
path: '/about'
title: 'About myself'
---

###Here's a brief summary of myself:

<div class="main">

![Beep](../images/dos3.0.png)

My childhood involved bulky PC towers, monitors piled high, cords dangling around like a sci-fi cliché. My father was a PC-fanboy back in the 80s, he even taught me DOS commands when I was 5. Windows 3.0, 28.8K modem dialing to the BBS servers, installing programs using stacks of floppy disks, cleaning dirty mouse scrollballs, keyboards with no Windows keys...all good memories that I cherish.

Growing up, I used Netscape every day, favoured low-end PC gaming, and was a flash games enthusiast (Kongregate anyone?). Spent a whole month during one of my summer vacations on Solitaire Las Vegas mode three card. (It is nearly impossible to make a positive balance after a few games). Nothing technically remarkable, got hacked by annoying friends through ICQ, CD-Rom drives got popped in and out, all I did was unplug the phone cable for a few days. That was it.

However, this is not a story of how deep I have sunken into the computer world.

As a kind of person who believes it is more worthwhile to try different things, I intentionally worked in some not-so-computery fields after college. Starting with trading (which brought me to printing), printing (which brought me to advertising), then graphic design, and then product design, and even three years running a business (a web development shop and team of three).

Eventually I made my way back into the programming world. 'Back' because my last formal training was during my teenage years studying computer science. Of course it was completely different then, I learned Pascal, and we do not use `goto` anymore.

It might sound odd, but I enjoy thinking. Some thoughts can be really impractical and silly, for example, it is fun to find words that match with generated license plate combinations, especially when they have an interesting sequence. I also enjoy making acronyms for no reason (FNR). Otherwise, I am happy to focus my brain by learning and problem solving. Programming creates joyful "aha!" moments, and is the reason I love working with code everyday.

###Favourite things:

<div class="extras">

####Cooking

Improvisation at its finest. Taste is subjective. Moderately skilled at empty-fridge challenges.

####Bookstore Visiting

Looking at good books is good.

####Tweeting

My tweets are sweet, and not diabetic.

####Mechanical Keyboards

Love the clicks and clacks. It won't help with writing code, but it helps with the typing muscles. (I am writing this using my [Nyquist](https://keeb.io/products/nyquist-keyboard).)

####Music

Great to be adventurous! I enjoy almost any genre of music that is not overplayed/overpraised/overrated.

####Meeting local wildlife

Usually cats, squirrels, and bunnies.

###Contact
you can contact me at  
mr rondie at gmail dot com

</div>
