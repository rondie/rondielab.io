// import React from 'react'
// import { StaticQuery, graphql } from 'gatsby'
// import { Layout, Case } from '../components'
// import uuid from 'uuid'

// const Cases = () => (
//   <StaticQuery
//     query={graphql`
//       query CasesQuery {
//         allMarkdownRemark(
//           sort: { fields: frontmatter___order }
//           filter: { fileAbsolutePath: { regex: "/cases/" } }
//           limit: 1000
//         ) {
//           edges {
//             node {
//               frontmatter {
//                 path
//                 title
//                 published
//                 intro
//                 domain
//                 thumb {
//                   childImageSharp {
//                     fluid(maxWidth: 1080) {
//                       base64
//                       tracedSVG
//                       aspectRatio
//                       src
//                       srcSet
//                       srcWebp
//                       srcSetWebp
//                       sizes
//                       originalImg
//                       originalName
//                       presentationWidth
//                       presentationHeight
//                     }
//                   }
//                 }
//               }
//             }
//           }
//         }
//       }
//     `}
//     render={data => {
//       const cases = data.allMarkdownRemark.edges
//       return (
//         <Layout>
//           {cases.map(
//             cas =>
//               cas.node.frontmatter.published && (
//                 <Case key={uuid.v4()} data={cas} />
//               )
//           )}
//         </Layout>
//       )
//     }}
//   />
// )

// export default Cases
