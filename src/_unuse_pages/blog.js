// import React from 'react'
// import { StaticQuery, graphql } from 'gatsby'
// import { Layout, SEO, Post } from '../components'
// import uuid from 'uuid'

// const Posts = () => (
//   <StaticQuery
//     query={graphql`
//       query PostsQuery {
//         allMarkdownRemark(
//           sort: { order: DESC, fields: [frontmatter___date] }
//           filter: { fileAbsolutePath: { regex: "/posts/" } }
//           limit: 1000
//         ) {
//           edges {
//             node {
//               excerpt(format: HTML, pruneLength: 250, truncate: true)
//               frontmatter {
//                 path
//                 title
//                 published
//                 thumb {
//                   childImageSharp {
//                     fluid(maxWidth: 300, maxHeight: 400) {
//                       base64
//                       tracedSVG
//                       aspectRatio
//                       src
//                       srcSet
//                       srcWebp
//                       srcSetWebp
//                       sizes
//                       originalImg
//                       originalName
//                       presentationWidth
//                       presentationHeight
//                     }
//                   }
//                 }
//               }
//             }
//           }
//         }
//       }
//     `}
//     render={data => {
//       const posts = data.allMarkdownRemark.edges
//       return (
//         <Layout>
//           {posts.map(
//             post =>
//               post.node.frontmatter.published && (
//                 <Post key={uuid.v4()} data={post} />
//               )
//           )}
//         </Layout>
//       )
//     }}
//   />
// )

// export default Posts
