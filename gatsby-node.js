// const path = require('path')

// exports.createPages = async ({ actions, graphql }) => {
//   const { createPage } = actions

//   const postTemplate = path.resolve(`src/templates/post.js`)
//   // const caseTemplate = path.resolve(`src/templates/case.js`)

//   await graphql(`
//     {
//       allMarkdownRemark(
//         sort: { order: DESC, fields: [frontmatter___date] }
//         filter: { fileAbsolutePath: { regex: "/posts/" } }
//         limit: 1000
//       ) {
//         edges {
//           node {
//             html
//             frontmatter {
//               date
//               path
//               title
//               published
//               thumb {
//                 childImageSharp {
//                   fluid(maxWidth: 800) {
//                     base64
//                     tracedSVG
//                     aspectRatio
//                     src
//                     srcSet
//                     srcWebp
//                     srcSetWebp
//                     sizes
//                     originalImg
//                     originalName
//                     presentationWidth
//                     presentationHeight
//                   }
//                 }
//               }
//             }
//           }
//         }
//       }
//     }
//   `).then(result => {
//     if (result.errors) {
//       return Promise.reject(result.errors)
//     }

//     result.data.allMarkdownRemark.edges.forEach(({ node }) => {
//       createPage({
//         path: node.frontmatter.path,
//         component: postTemplate,
//         context: {}, // additional data can be passed via context
//       })
//     })
//   })

// await graphql(`
//   {
//     allMarkdownRemark(
//       sort: { order: DESC, fields: [frontmatter___date] }
//       filter: { fileAbsolutePath: { regex: "/cases/" } }
//       limit: 1000
//     ) {
//       edges {
//         node {
//           frontmatter {
//             date
//             path
//             title
//           }
//         }
//       }
//     }
//   }
// `).then(result => {
//   if (result.errors) {
//     return Promise.reject(result.errors)
//   }

//   result.data.allMarkdownRemark.edges.forEach(({ node }) => {
//     createPage({
//       path: node.frontmatter.path,
//       component: caseTemplate,
//       context: {}, // additional data can be passed via context
//     })
//   })
// })
//   return
// }
